

export default function Highlights() {
	return (
	<div className="container-fluid mb-4">
		<div className="row">
			<div className="col-10 col-md-4">
				 <div className="card">
				   <div className="card-body">
				     <h5 className="card-title">Learn From Home</h5>
				     <p className="card-text">Lorem ipsum dolor, sit amet consectetur, adipisicing elit. Ea, nemo nulla itaque nobis dignissimos quibusdam unde fugit, voluptate voluptatem vero vitae expedita cupiditate blanditiis eum tempore quisquam molestias at repellat.</p>
				   </div>
				 </div>
			</div>
			<div className="col-10 col-md-4">
				 <div className="card">
				   <div className="card-body">
				     <h5 className="card-title">Study Now, Pay Later</h5>
				     <p className="card-text">Lorem ipsum dolor, sit amet consectetur, adipisicing elit. Ea, nemo nulla itaque nobis dignissimos quibusdam unde fugit, voluptate voluptatem vero vitae expedita cupiditate blanditiis eum tempore quisquam molestias at repellat.</p>
				   </div>
				 </div>
			</div>
			<div className="col-10 col-md-4">
				 <div className="card">
				   <div className="card-body">
				     <h5 className="card-title">Be Part of our Community</h5>
				     <p className="card-text">Lorem ipsum dolor, sit amet consectetur, adipisicing elit. Ea, nemo nulla itaque nobis dignissimos quibusdam unde fugit, voluptate voluptatem vero vitae expedita cupiditate blanditiis eum tempore quisquam molestias at repellat.</p>
				   </div>
				 </div>
			</div>
		</div>
	</div>
	)
}