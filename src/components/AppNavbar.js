import {Navbar, Nav} from "react-bootstrap"

export default function AppNavbar() {
  return (
    <Navbar bg="primary" expand="lg">
      <Navbar.Brand href="#home">Course Booking</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="me-auto">
          <Nav.Link href="#home">Home</Nav.Link>
          <Nav.Link href="#link">Link</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
