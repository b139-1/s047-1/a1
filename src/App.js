import 'bootstrap/dist/css/bootstrap.min.css';
import {Fragment} from "react"
import {Container} from "react-bootstrap"
import AppNavbar from "./components/AppNavbar.js"
import Home from "./pages/Home.js"
import Courses from "./pages/Courses.js"
import Register from "./pages/Register.js"
import Login from './pages/Login.js'

export default function App() {
  return (
    <Fragment>
      <AppNavbar/>
      <Container fluid>
        <Home/>
        <Courses/>
        <Register/>
        <Login/>
      </Container>
    </Fragment>
  )
}