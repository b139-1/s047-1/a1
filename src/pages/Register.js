import {Container, Form, Button} from "react-bootstrap"
import {useState, useEffect} from "react"

export default function Register() {

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [cpw, setCpw] = useState("")
	const [isDisabled, setIsDisabled] = useState(false)

	useEffect(() => {
		if(email !== '' && password !== '' && cpw !== '' && password === cpw) {
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [email, password, cpw])

	const register = (event) => {
		event.preventDefault()
		alert("Succesful registration\nLogin to your account now!")
		setEmail("")
		setPassword("")
		setCpw("")
	}

	return (
		<Container>
			<Form 
				className="border p-3 mb-4"
				onSubmit={(event) => register(event)}
			>
			  <Form.Group className="mb-3" controlId="email">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control
		    		type="email"
		    		placeholder="Enter email"
		    		value={email}
		    		onChange={(event) => setEmail(event.target.value)}
			    />
			    <Form.Text className="text-muted">
			    </Form.Text>
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="password">
			    <Form.Label>Password</Form.Label>
			    <Form.Control
		    		type="password"
		    		placeholder="Password"
		    		value={password}
		    		onChange={(event) => setPassword(event.target.value)}
			    />
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="cpw">
			    <Form.Label>Confirm Password</Form.Label>
			    <Form.Control
		    		type="password"
		    		placeholder="Verify Password"
		    		value={cpw}
		    		onChange={(event) => setCpw(event.target.value)}
			    />
			  </Form.Group>
			  <Button variant="primary" type="submit" disabled={isDisabled}>
			    Submit
			  </Button>
			</Form>
		</Container>
	)
}