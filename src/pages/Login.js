import {Container, Form, Button} from "react-bootstrap"
import {useState, useEffect} from "react"

export default function Login() {
	
	const [logEmail, setLogEmail] = useState("")
	const [logPassword, setLogPassword] = useState("")
	const [isDisabled, setIsDisabled] = useState(false)
	
	useEffect(() => {
			if(logEmail !== '' && logPassword !== '') {
				setIsDisabled(false)
			} else {
				setIsDisabled(true)
			}
		}, [logEmail, logPassword])
	
	const login = (event) => {
			event.preventDefault()
			setLogEmail("")
			setLogPassword("")
			alert("Logged in succesfully!")
		}


	return (
		<Container>
			<Form className="border p-3 mb-4" onSubmit={(event) => login(event)}>
			  <Form.Group className="mb-3" controlId="logEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control type="email" placeholder="Enter email" value={logEmail} onChange={(event) => setLogEmail(event.target.value)}/>
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="logPassword">
			    <Form.Label>Password</Form.Label>
			    <Form.Control type="password" placeholder="Password" value={logPassword} onChange={(event) => setLogPassword(event.target.value)}/>
			  </Form.Group>
			  <Button variant="success" type="submit" disabled={isDisabled}>
			    Login
			  </Button>
			</Form>
		</Container>
	)
}